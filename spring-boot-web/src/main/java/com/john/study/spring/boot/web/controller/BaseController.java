package com.john.study.spring.boot.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jiangguangtao on 2017/10/25.
 */
@RestController
public class BaseController {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Value("${spring.cloud.consul.discovery.instance-id}")
    private String instanceId;

    @GetMapping(value = {"/", "/who"})
    public String who() {
        return "服器ID："+instanceId+"\n<br/> 服务器时间："+sdf.format(new Date());
    }

    @ResponseBody
    @RequestMapping("/ping")
    public String ping() {
        return "Ok";
    }

    @GetMapping("/hi")
    public String hi(String name) {
        return "Hello " + name + "!";
    }
}
